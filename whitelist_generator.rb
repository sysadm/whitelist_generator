#!/usr/bin/env ruby -W0

require 'roo'

whitelists, intersection = [], []
Dir.glob('xlsx/*.xlsx') do |xlsx|
  xlsx = Roo::Spreadsheet.open(xlsx, extension: :xlsx)
  all_emails = xlsx.sheet(0).column(1).compact.map{ |m| m.chomp(',').strip.chomp(',') }
  exclude_list = xlsx.sheet(0).column(2).compact.map{ |m| m.chomp(',').strip.chomp(',') }
  intersection << (all_emails & exclude_list)
  whitelists << (all_emails - exclude_list)
end
intersection = intersection.flatten.compact
puts "Common excluded elements:\n"; intersection.each{|i| puts "#{i}\n"} if intersection.size > 0
file = File.open("white_list.csv", "w+b", 0644)
file.write (whitelists.flatten.uniq.join(",\n") + ",\n")
file.close
